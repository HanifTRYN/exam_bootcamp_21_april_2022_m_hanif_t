from odoo import api, fields, models

class TrainUser(models.Model):
    _inherit = 'res.partner'

    user_state = fields.Selection([('passenger', 'Penumpang'), ('machinist', 'Masinis')], string='Status User', help='Jenis User')
    identity = fields.Char(string='Nomor Identitas', required=True)
    gender = fields.Selection([('male', 'Pria'), ('female', 'Wanita')], string='Jenis Kelamin', help='Jenis Kelamin', required=True)
    born_date = fields.Date(string='Tanggal Lahir', required=True)