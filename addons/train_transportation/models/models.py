# -*- coding: utf-8 -*-

# from email.policy import default
# import string
# from unicodedata import name
from odoo import models, fields, api
from datetime import timedelta, datetime, date, time


class TrainCity(models.Model):
    _name = 'train.city'
    _description = 'Train City'

    name = fields.Char(string='Nama Kota')
    code = fields.Char(string='Kode Kota')

class TrainStation(models.Model):
    _name = 'train.station'
    _description = 'Train Station'

    code = fields.Char(string='Kode Stasiun')
    name = fields.Char(string='Nama Stasiun')
    station_city_id = fields.Many2one('train.city', string='Relasi Kota dan Stasiun')
    address = fields.Text(string='address')

class Train(models.Model):
    _name = 'train.train'
    _description = 'Train'

    name = fields.Char(string='Nama Kereta')
    code = fields.Char(string='Kode Kereta')
    train_capacity = fields.Integer(string='Kapasitas Kereta')
    train_state = fields.Selection([('ready', 'Ready'), ('not_ready', 'Not Ready'), ('maintenance', 'Maintenance')], string='Status Kereta', help='Kesiapan Kereta')
    train_active = fields.Boolean(string='Aktif', default=True)
    schedule_line = fields.One2many('train.schedule', 'train_id', string='Jadwal Kereta')   
    
class TrainSchedule(models.Model):
    _name = 'train.schedule'
    _description = 'Train Schedule'

    @api.depends('schedule_time', 'duration')
    def get_arrival_time(self):
        for time in self:
            if not time.schedule_time:
                time.arrival_time = time.schedule_time
                continue

            start = fields.Datetime.from_string(time.schedule_time)
            time.arrival_time = start + timedelta(hours=time.duration)
        

    def set_arrival_time(self):
        for time in self:
            if not (time.schedule_time and time.arrival_time):
                continue

            schedule_time = fields.Datetime.from_string(time.schedule_time)
            arrival_time = fields.Datetime.from_string(time.arrival_time)
            time.duration = (arrival_time - schedule_time).day + 1

    code = fields.Char(string='Kode Jadwal', readonly=True, default='/')
    origin_id = fields.Many2one('train.station', string='Asal')
    destination_id = fields.Many2one('train.station', string='Tujuan')
    schedule_time = fields.Datetime(string='Jam')
    duration = fields.Float(string='Durasi')
    arrival_time = fields.Datetime(string='Jam Sampai', compute='get_arrival_time', inverse='set_arrival_time', readonly=True)
    train_id = fields.Many2one('train.train', string='Nama Kereta', ondelete='cascade')
    capacity = fields.Integer(string='Kapasitas Kereta', related='train_id.train_capacity', store=True)

    @api.model
    def create(self, vals):
        vals['code'] = self.env['ir.sequence'].next_by_code('train.schedule')
        return super(TrainSchedule, self).create(vals)




    
    

    