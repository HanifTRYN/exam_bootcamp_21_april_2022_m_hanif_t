# -*- coding: utf-8 -*-
{
    'name': "train_transportation",

    'summary': """Modul Tentang Transportasi Kereta Api""",

    'description': """Modul Transportasi Kereta Api""",

    'author': "Muhammad Hanif Triyana",
    'website': "mhanif117@gmail.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/15.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'views/views.xml',
        'views/schedule_code.xml',
        'wizard/schedule_wizard_views.xml',
        'views/templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
