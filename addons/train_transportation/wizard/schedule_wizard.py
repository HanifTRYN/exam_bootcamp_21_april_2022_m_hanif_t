from odoo import api, fields, models

class ScheduleWizard(models.TransientModel):
    _name = 'schedule.wizard'
    _description = 'Schedule Wizard'

    def _default_sesi(self):
        return self.env['train.train'].browse(self._context.get('active_ids'))
    
    train_id = fields.Many2one('train.train', string='Nama Kereta')
    schedule_ids = fields.Many2many('train.schedule', string='Jadwal Kereta')

    def tambah_schedule(self):
        self.train_id.schedule_ids |= self.schedule_ids
    